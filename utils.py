#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from sympy import latex


def latex_vector_xy(symbolname):
    return r'\begin{bmatrix}' \
           + latex(eval(symbolname + 'x')) + r' \\ ' \
           + latex(eval(symbolname + 'y')) + r' \\ ' \
           + r'0 \end{bmatrix}'
