#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from sympy import *

from utils import *

alpha, beta, dalpha, dbeta = symbols('alpha, beta, dalpha, dbeta')  # definicja współrzędnych uogólnionych
a, b, c, r, M, m = symbols('a, b, c, r, M, m')  # definicja wymiarów

# Punkt 4
print('\nPunkt 4')
Bx, By, vBx, vBy, vDBx, vDBy, vDx, vDy, vD2, I0, ID, T1, T2, T = \
    symbols('Bx, By, vBx, vBy, vDBx, vDBy, vDx, vDy, vD2, I0, ID, T1, T2, T')

Bx_def = a * cos(alpha) + b * sin(alpha)
By_def = a * sin(alpha) - b * cos(alpha)
print(Eq(Bx, Bx_def))

print(Eq(vBx, Derivative(Bx, alpha) * dalpha))
vBx_def = diff(Bx_def, alpha) * dalpha
vBy_def = diff(By_def, alpha) * dalpha
print(Eq(vBx, vBx_def))

vDBx_def = -r * sin(beta) * dbeta
vDBy_def = r * cos(beta) * dbeta
print(Eq(vDBx, vDBx_def))

vDx_def = vBx + vDBx
vDy_def = vBy + vDBy
print(Eq(vDx, vDx_def))
vDx_def = vDx_def.subs(vBx, vBx_def).subs(vDBx, vDBx_def)
vDy_def = vDy_def.subs(vBy, vBy_def).subs(vDBy, vDBy_def)
print(Eq(vDx, vDx_def))

vD2_def = vDx ** 2 + vDy ** 2  # kwadrat prędkości (skalar)
print(Eq(vD2, vD2_def))
vD2_def = vD2_def.subs(vDx, vDx_def).subs(vDy, vDy_def).simplify()

T1_def = Rational(1, 2) * I0 * dalpha ** 2  # energia kinetyczna ramy
print(Eq(T1, T1_def))
T2_def = Rational(1, 2) * ID * dbeta ** 2 + Rational(1, 2) * m * vD2  # energia kinetyczna koła
print(Eq(T2, T2_def))
print(Eq(T, T1 + T2))
T_def = T1_def + T2_def
print(Eq(T, T_def))

# Punkt 5
print('\nPunkt 5')
Iac0, IbA, Ib0, Mac, Mb = symbols('Iac0, IbA, Ib0, Mac, Mb')

Mac_def = M * (a + c) / (a + b + c)
print(Eq(Mac, Mac_def))
Mb_def = M * b / (a + b + c)
print(Eq(Mb, Mb_def))

Iac0_def = Rational(1, 3) * Mac * (a + c) ** 2  # moment prostego pręta
print(Eq(Iac0, Iac0_def))
IbA_def = Rational(1, 3) * Mb * b ** 2  # moment bocznego pręta
print(Eq(IbA, IbA_def))
Ib0_def = IbA + Mb * a ** 2  # twierdzenie Steinera
print(Eq(Ib0, Ib0_def))
Ib0_def = Ib0_def.subs(IbA, IbA_def).factor()
print(Eq(Ib0, Ib0_def))
I0_def = Iac0 + Ib0
print(Eq(I0, I0_def))
I0_def = I0_def.subs(Iac0, Iac0_def).subs(Ib0, Ib0_def).subs(Mac, Mac_def).subs(Mb, Mb_def).factor()
print(Eq(I0, I0_def))

# Punkt 6
print('\nPunkt 6')
print(Eq(vDx, vDx_def))  # vDx, vDy obliczone przy okazji punktu 4

# Punkt 7
print('\nPunkt 7')
k1, k2, k3, V1, V2, V, d1, d2, d3, Cy, Dx = symbols('k1, k2, k3, V1, V2, V, d1, d2, d3, Cy, Dx')

V1_def = Rational(1, 2) * k2 * d2 ** 2
print(Eq(V1, V1_def))
with evaluate(False):
    V2_def = Rational(1, 2) * k1 * d1 ** 2 + Rational(1, 2) * k3 * d3 ** 2
print(Eq(V2, V2_def))
Dx_def = Bx + r * cos(beta)
print(Eq(Dx, Dx_def))
d1_def = Dx - (a + r)
print(Eq(d1, d1_def))
d1_def = d1_def.subs(Dx, Dx_def).subs(Bx, Bx_def)
print(Eq(d1, d1_def))
Cy_def = (a + c) * sin(alpha)
print(Eq(Cy, Cy_def))
d2_def = Cy
print(Eq(d2, d2_def))
d2_def = d2_def.subs(Cy, Cy_def)
print(Eq(d2, d2_def))
d3_def = beta - alpha
print(Eq(d3, d3_def))

V1_def = V1_def.subs(d2, d2_def)
print(Eq(V1, V1_def))
V2_def = V2_def.subs(d1, d1_def).subs(d3, d3_def)
print(Eq(V2, V2_def))
V_def = V1 + V2
print(Eq(V, V_def))
V_def = V_def.subs(V1, V1_def).subs(V2, V2_def)
print(Eq(V, V_def))

# Punkt 8
print('\nPunkt 8')
Tdalpha, Tdbeta = symbols('Tdalpha, Tdbeta')

ID_def = Rational(1, 2) * m * r ** 2
print(Eq(ID, ID_def))
T_def = T_def.subs(I0, I0_def).subs(ID, ID_def).subs(vD2, vD2_def)
print(Eq(T, T_def))

Tdalpha_def = Derivative(T, dalpha)
print(Eq(Tdalpha, Tdalpha_def))
Tdalpha_def = Tdalpha_def.subs(T, T_def).doit()
print(Eq(Tdalpha, Tdalpha_def))
Tdbeta_def = Derivative(T, dbeta)
print(Eq(Tdbeta, Tdbeta_def))
Tdbeta_def = Tdbeta_def.subs(T, T_def).doit()
print(Eq(Tdbeta, Tdbeta_def))

# Punkt 9
print('\nPunkt 9')
Talpha, Tbeta = symbols('Talpha, Tbeta')

Talpha_def = Derivative(T, alpha)
print(Eq(Talpha, Talpha_def))
Talpha_def = Talpha_def.subs(T, T_def).doit()
print(Eq(Talpha, Talpha_def))
Tbeta_def = Derivative(T, beta)
print(Eq(Tbeta, Tbeta_def))
Tbeta_def = Tbeta_def.subs(T, T_def).doit()
print(Eq(Tbeta, Tbeta_def))

# Punkt 10
print('\nPunkt 10')
Valpha, Vbeta = symbols('Valpha, Vbeta')

Valpha_def = Derivative(V, alpha)
print(Eq(Valpha, Valpha_def))
Valpha_def = Valpha_def.subs(V, V_def).doit()
print(Eq(Valpha, Valpha_def))
Vbeta_def = Derivative(V, beta)
print(Eq(Vbeta, Vbeta_def))
Vbeta_def = Vbeta_def.subs(V, V_def).doit()
print(Eq(Vbeta, Vbeta_def))

tex_names = {
    dalpha: r'\dot{\alpha}',
    dbeta: r'\dot{\beta}',
    Bx: 'B_x',
    By: 'B_y',
    vBx: 'v_{Bx}',
    vBy: 'v_{By}',
    vDBx: 'v_{D|Bx}',
    vDBy: 'v_{D|By}',
    vDx: 'v_{Dx}',
    vDy: 'v_{Dy}',
    I0: 'I_0',
    ID: 'I_D',
    T1: 'T_1',
    T2: 'T_2',
}
# print(latex_vector_xy('vD'))
