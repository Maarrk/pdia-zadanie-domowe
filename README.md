# pdia-zadanie-domowe

Projekt wykonany na przedmiot [Podstawy Drgań i Aeroelastyczności](https://www.meil.pw.edu.pl/zm/ZM/Dydaktyka/Prowadzone-przedmioty/Podstawy-drgan-i-aeroelastycznosci), realizowany na Wydziale Mechanicznym Energetyki i Lotnictwa Politechniki Warszawskiej w semestrze zimowym 2019/2020.

# Wykorzystane narzędzia

- Kontrola wersji: [Git](https://git-scm.com/)
- Dokumentacja: [LaTeX](https://www.latex-project.org/)
- Obliczenia:
    - Język interpretowany [Python 3.7](https://www.python.org/)
    - Biblioteki obliczeniowe [sympy](http://www.sympy.org/)
- Narzędzia:
    - Edytor LaTeX [TeXstudio](https://www.texstudio.org/)
    - Edytor Python [PyCharm](https://www.jetbrains.com/pycharm/)

# TODO

- symboliczne rozwiązanie pozostałych punktów
- eksport równań do pliku
    - nazywanie kolejnych równań
    - nadpisanie konkretnych równań (nie psuje opisów pomiędzy)
    - ¿czy każde równanie w swoim pliku w folderze `auto`?
- rysunek układu